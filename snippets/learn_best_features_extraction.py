import pandas as pd
from fets.math import TSIntegrale;
from polaris.learning.feature.extraction import extract_best_features, create_list_of_transformers

# Create a small list of two transformers which will generate two different pipelines
transformers = create_list_of_transformers(["5min", "15min"], TSIntegrale)

# Extract the best features of the two pipelines
A = extract_best_features("data/power--2014-01-01_2015-01-01.csv",
                          transformers,
                          target_column="NPWD2372",
                          time_unit="ms")

# A[0] is the FeatureImportanceOptimization object
# from polaris.learning.feature.selection
print(A[0].best_features)

