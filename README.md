# Polaris Playground

Repository to hold any tools, files, resources, notebooks, etc, related to the polaris machine learning project.

## Running notebooks

Run inside the main virtual environment:

```bash
# Install dependencies and activate the environment
pipenv sync
pipenv shell

# View notebooks...
jupyter notebook

# ... or run a snippet
python3 snippets/learn_best_features_extraction.py
```

## Reading/Writing diagrams

### Using gaphor

The simplest approach for drawing UML diagrams is
```
pip install gaphor
```

Then open gaphor files 
```
gaphor wherever/file.gaphor
```

### Using drawio

There is a container for drawio you can use to run a local drawio instance.

see: https://hub.docker.com/r/jgraph/drawio
